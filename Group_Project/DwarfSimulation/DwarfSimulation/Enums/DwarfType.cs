﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DwarfSimulation
{
    internal enum DwarfType
    {
        Father,
        Single,
        Lazy,
        Suicider
    }
}
