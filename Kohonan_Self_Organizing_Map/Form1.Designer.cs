﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series61 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series62 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series63 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series64 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series65 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series66 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series67 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series68 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series69 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series70 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series71 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series72 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series73 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series74 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series75 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series76 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series77 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series78 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series79 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series80 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(577, 444);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(117, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Start";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.ForeColor = System.Drawing.SystemColors.Control;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(124, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Mapa Samoorganizująca";
            // 
            // chart1
            // 
            chartArea4.AxisX.Interval = 5D;
            chartArea4.AxisX.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.DashDot;
            chartArea4.AxisX.Maximum = 20D;
            chartArea4.AxisX.Minimum = 0D;
            chartArea4.AxisX2.LineColor = System.Drawing.Color.White;
            chartArea4.AxisY.Interval = 5D;
            chartArea4.AxisY.Maximum = 20D;
            chartArea4.AxisY.Minimum = 0D;
            chartArea4.BorderWidth = 0;
            chartArea4.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea4);
            legend4.Name = "Legend1";
            this.chart1.Legends.Add(legend4);
            this.chart1.Location = new System.Drawing.Point(13, 29);
            this.chart1.Name = "chart1";
            series61.ChartArea = "ChartArea1";
            series61.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series61.Label = "ajayeb";
            series61.Legend = "Legend1";
            series61.MarkerSize = 12;
            series61.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series61.Name = "ajayeb";
            series62.ChartArea = "ChartArea1";
            series62.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series62.IsValueShownAsLabel = true;
            series62.Label = "ajmal";
            series62.Legend = "Legend1";
            series62.MarkerSize = 12;
            series62.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series62.Name = "ajmal";
            series63.ChartArea = "ChartArea1";
            series63.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series63.IsValueShownAsLabel = true;
            series63.Label = "amreaj";
            series63.Legend = "Legend1";
            series63.MarkerSize = 12;
            series63.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series63.Name = "amreaj";
            series64.ChartArea = "ChartArea1";
            series64.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series64.IsValueShownAsLabel = true;
            series64.Label = "aood";
            series64.Legend = "Legend1";
            series64.MarkerSize = 12;
            series64.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series64.Name = "aood";
            series65.ChartArea = "ChartArea1";
            series65.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series65.IsValueShownAsLabel = true;
            series65.Label = "asgar_ali";
            series65.Legend = "Legend1";
            series65.MarkerSize = 12;
            series65.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series65.Name = "asgar_ali";
            series66.ChartArea = "ChartArea1";
            series66.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series66.IsValueShownAsLabel = true;
            series66.Label = "bukhoor";
            series66.Legend = "Legend1";
            series66.MarkerSize = 12;
            series66.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series66.Name = "bukhoor";
            series67.ChartArea = "ChartArea1";
            series67.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series67.IsValueShownAsLabel = true;
            series67.Label = "burberrry";
            series67.Legend = "Legend1";
            series67.MarkerSize = 12;
            series67.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series67.Name = "burberrry";
            series68.ChartArea = "ChartArea1";
            series68.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series68.IsValueShownAsLabel = true;
            series68.Label = "dehenalaod";
            series68.Legend = "Legend1";
            series68.MarkerSize = 12;
            series68.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series68.Name = "dehenalaod";
            series69.ChartArea = "ChartArea1";
            series69.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series69.IsValueShownAsLabel = true;
            series69.Label = "junaid";
            series69.Legend = "Legend1";
            series69.MarkerSize = 12;
            series69.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series69.Name = "junaid";
            series70.ChartArea = "ChartArea1";
            series70.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series70.IsValueShownAsLabel = true;
            series70.Label = "kausar";
            series70.Legend = "Legend1";
            series70.MarkerSize = 12;
            series70.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series70.Name = "kausar";
            series71.ChartArea = "ChartArea1";
            series71.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series71.IsValueShownAsLabel = true;
            series71.Label = "rose";
            series71.Legend = "Legend1";
            series71.MarkerSize = 12;
            series71.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series71.Name = "rose";
            series72.ChartArea = "ChartArea1";
            series72.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series72.IsValueShownAsLabel = true;
            series72.Label = "solidmusk";
            series72.Legend = "Legend1";
            series72.MarkerSize = 12;
            series72.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series72.Name = "solidmusk";
            series73.ChartArea = "ChartArea1";
            series73.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series73.IsValueShownAsLabel = true;
            series73.Label = "TeaTreeOil";
            series73.Legend = "Legend1";
            series73.MarkerSize = 12;
            series73.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series73.Name = "TeaTreeOil";
            series74.ChartArea = "ChartArea1";
            series74.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series74.IsValueShownAsLabel = true;
            series74.Label = "raspberry";
            series74.Legend = "Legend1";
            series74.MarkerSize = 12;
            series74.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series74.Name = "raspberry";
            series75.ChartArea = "ChartArea1";
            series75.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series75.IsValueShownAsLabel = true;
            series75.Label = "RoseMusk";
            series75.Legend = "Legend1";
            series75.MarkerSize = 12;
            series75.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series75.Name = "RoseMusk";
            series76.ChartArea = "ChartArea1";
            series76.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series76.IsValueShownAsLabel = true;
            series76.Label = "strawberry";
            series76.Legend = "Legend1";
            series76.MarkerSize = 12;
            series76.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series76.Name = "strawberry";
            series77.ChartArea = "ChartArea1";
            series77.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series77.IsValueShownAsLabel = true;
            series77.Label = "constrected2";
            series77.Legend = "Legend1";
            series77.MarkerSize = 12;
            series77.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series77.Name = "constrected2";
            series78.ChartArea = "ChartArea1";
            series78.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series78.IsValueShownAsLabel = true;
            series78.Label = "carolina_herrera";
            series78.Legend = "Legend1";
            series78.MarkerSize = 12;
            series78.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series78.Name = "carolina_herrera";
            series79.ChartArea = "ChartArea1";
            series79.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series79.IsValueShownAsLabel = true;
            series79.Label = "oudh_ma\'alattar";
            series79.Legend = "Legend1";
            series79.MarkerSize = 12;
            series79.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series79.Name = "oudh_ma\'alattar";
            series80.ChartArea = "ChartArea1";
            series80.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series80.IsValueShownAsLabel = true;
            series80.Label = "constrected";
            series80.Legend = "Legend1";
            series80.MarkerSize = 12;
            series80.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series80.Name = "constrected";
            this.chart1.Series.Add(series61);
            this.chart1.Series.Add(series62);
            this.chart1.Series.Add(series63);
            this.chart1.Series.Add(series64);
            this.chart1.Series.Add(series65);
            this.chart1.Series.Add(series66);
            this.chart1.Series.Add(series67);
            this.chart1.Series.Add(series68);
            this.chart1.Series.Add(series69);
            this.chart1.Series.Add(series70);
            this.chart1.Series.Add(series71);
            this.chart1.Series.Add(series72);
            this.chart1.Series.Add(series73);
            this.chart1.Series.Add(series74);
            this.chart1.Series.Add(series75);
            this.chart1.Series.Add(series76);
            this.chart1.Series.Add(series77);
            this.chart1.Series.Add(series78);
            this.chart1.Series.Add(series79);
            this.chart1.Series.Add(series80);
            this.chart1.Size = new System.Drawing.Size(681, 409);
            this.chart1.TabIndex = 3;
            this.chart1.Text = "chart1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.label2.Enabled = false;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Location = new System.Drawing.Point(160, 202);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(257, 46);
            this.label2.TabIndex = 4;
            this.label2.Text = "Trwa uczenie";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(706, 479);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Label label2;
    }
}

